package steptest

import (
	"testing"
)

func TestOne(t *testing.T) {
	cases := []struct {
		allowed []int
		steps   int
		exp     int
	}{
		{allowed: []int{1}, steps: 1, exp: 1},
		{allowed: []int{1}, steps: 2, exp: 1},
		{allowed: []int{1}, steps: 2, exp: 1},
		{allowed: []int{2}, steps: 1, exp: 0},
		{allowed: []int{1, 2}, steps: 1, exp: 1},
		{allowed: []int{1, 2}, steps: 2, exp: 2},
		{allowed: []int{1}, steps: 10, exp: 1},
	}

	for _, c := range cases {
		got := Ways(c.steps, c.allowed)
		if c.exp != got {
			t.Errorf("expected %d, got %d", c.exp, got)
		}
	}
}

func TestTwo(t *testing.T) {
	cases := []struct {
		allowed []int
		steps   int
		exp     int
	}{
		{allowed: []int{1, 2}, steps: 3, exp: 3},
		{allowed: []int{1, 2, 3}, steps: 3, exp: 4},
	}

	for _, c := range cases {
		got := Ways(c.steps, c.allowed)
		if c.exp != got {
			t.Errorf("expected %d, got %d", c.exp, got)
		}
	}
}

func TestPath(t *testing.T) {
	cases := []struct {
		allowed []int
		steps   int
		exp     int
	}{
		// {allowed: []int{1}, steps: 1, exp: 1},
		{allowed: []int{1, 2, 3}, steps: 3, exp: 4},
		// {allowed: []int{1, 2, 3}, steps: 3, exp: 4},
		{allowed: []int{1}, steps: 1, exp: 1},
		{allowed: []int{1}, steps: 2, exp: 1},
		{allowed: []int{1}, steps: 2, exp: 1},
		{allowed: []int{2}, steps: 1, exp: 0},
		{allowed: []int{1, 2}, steps: 1, exp: 1},
		{allowed: []int{1, 2}, steps: 2, exp: 2},
		{allowed: []int{1, 2, 5}, steps: 10, exp: 1},
	}

	for _, c := range cases {
		p := Paths(c.steps, c.allowed)
		_ = p
		t.Logf("steps %d, allowed: %v\n", c.steps, c.allowed)
		t.Logf("\n%s\n", PrintPaths(p))
		n := CountPaths(p)
		if c.exp != n {
			t.Errorf("expected %d, got %d", c.exp, n)
		}
	}
}

func TestPathJSON(t *testing.T) {
	cases := []struct {
		allowed []int
		steps   int
		exp     int
	}{
		{allowed: []int{1, 2, 5}, steps: 10, exp: 1},
	}

	for _, c := range cases {
		p := Paths(c.steps, c.allowed)
		_ = p
		t.Logf("steps %d, allowed: %v\n", c.steps, c.allowed)
		t.Log(Marshal(p))
	}
}
