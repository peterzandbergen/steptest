package steptest

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"
	"encoding/json"
)

var ErrNoWay = errors.New("no way found")

type Step struct {
	Steps int `json:"steps"`
	Next  []*Step `json:"next,omitempty"`
}

func ways(steps int, allowedSteps []int, step int) (nWays int) {
	for _, s := range allowedSteps {
		switch {
		case s == steps:
			nWays++
		case s < steps:
			nWays += ways(steps-s, allowedSteps, s)
		}
	}
	return
}

func paths(steps int, allowedSteps []int, st int) []*Step {
	var res []*Step
	for _, s := range allowedSteps {
		switch {
		case s == steps:
			res = append(res, &Step{
				Steps: s,
			})
		case s < steps:
			next := paths(steps-s, allowedSteps, s)
			if next != nil {
				res = append(res, &Step{
					Steps: s,
					Next:  next,
				})
			}
		}
	}
	return res
}

func Paths(steps int, allowedSteps []int) []*Step {
	return paths(steps, allowedSteps, 0)
}

func countPaths(paths []*Step) int {
	if paths == nil {
		return 1
	}
	var n int
	for _, s := range paths {
		n += countPaths(s.Next)
	}
	return n
}

func CountPaths(paths []*Step) int {
	if len(paths) == 0 {
		return 0
	}
	return countPaths(paths)
}

func Ways(steps int, allowedSteps []int) int {
	// Perform check on allowedSteps, should be > 0 and unique.
	return ways(steps, allowedSteps, 0)
}

func printPaths(w io.Writer, paths []*Step, level int) {
	if len(paths) == 0 {
		return
	}
	indent := strings.Repeat(" ", level*4)
	for _, p := range paths {
		fmt.Fprintf(w, "%sStep: %d\n", indent, p.Steps)
		printPaths(w, p.Next, level+1)
	}
}

func PrintPaths(paths []*Step) string {
	var b bytes.Buffer
	printPaths(&b, paths, 0)
	return b.String()
}

func Marshal(paths []*Step) string {
	b, err := json.MarshalIndent(paths, "", "    ")
	if err != nil {
		return ""
	}
	return string(b)
}